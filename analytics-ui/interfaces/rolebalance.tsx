interface RoleBalanceInterface {
    name: string;
    k: number;
    games: number;
    winrate: number;
    p: number;
}

class RoleBalance implements RoleBalanceInterface {
    name: string;
    k: number;
    games: number;
    winrate: number;
    p: number;
    constructor(name:string, k:number, games:number, winrate:number, p:number) {
        this.name = name;
        this.k = k;
        this.games = games;
        this.winrate = winrate;
        this.p = p;
    }
}