import { Html, Head, Main, NextScript } from 'next/document'
 
function Document() {
  return (
    <Html data-bs-theme="dark">
      <Head />
      <script src="https://kit.fontawesome.com/16eee79792.js" async></script>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}

export default Document;