import { Alert, Col, Container, Row } from "react-bootstrap"
import GlobalWin from "../components/globalwin"

export default function MainPage() {
    return (
    <>
        <Row style={{paddingTop: "16px"}} className="no-gutters">
            <Col>
                <GlobalWin/>
            </Col>
        </Row>
    </>
    )
}