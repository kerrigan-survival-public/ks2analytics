import { Card, Table } from "react-bootstrap";

export default function ClassTile(role: RoleBalance) {
    return (
        <>
        <Card style={{width: '12rem'}}>
        <Card.Body>
            <Card.Title>{role.name}</Card.Title>
        </Card.Body>
            <Table bordered>
                <tbody>
                    <tr>
                        <td>K-Value</td>
                        <td>{role.k}%</td>
                    </tr>
                    <tr>
                        <td>Sample Size</td>
                        <td>{role.games}</td>
                    </tr>
                    <tr>
                        <td>Winrate</td>
                        <td>{role.winrate}%</td>
                    </tr>
                    <tr>
                        <td>P-Value</td>
                        <td>{role.p}</td>
                    </tr>
                </tbody>
            </Table>
        </Card>
        </>
    )
}