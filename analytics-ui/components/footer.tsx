import Link from 'next/link';
import { ReactNode } from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';

export default function Footer(): ReactNode {
    return (
        <>
        <Navbar expand="lg" className="bg-body-tertiary" style={{position: 'fixed', bottom: 0, width: '100%'}}>
            <Container fluid>
                <div id='externals'>
                    <Link className="external-link" href={"https://gitlab.com/kerrigan-survival-public/ks2analytics"} target='_blank'><i className="fa-brands fa-gitlab"></i></Link>
                    <Link className="external-link" href={"https://discord.gg/ks2"} target='_blank'><i className="fa-brands fa-discord"></i></Link>
                </div>
            </Container>
        </Navbar>
        </>
    )
}