import { useState, useEffect } from 'react'
import { Container, Table } from 'react-bootstrap';
import useSWR from 'swr';

// @ts-ignore
const fetcher = (...args: string[]) => fetch(...args).then((res) => res.json())

export default function GlobalWin() {
    const { data, error, isLoading } = useSWR(`https://${process.env.NEXT_PUBLIC_API}/api/v1/game/count`, fetcher)
    
    if (isLoading) return <p>Loading...</p>
    return (
        <>
        <div style={{width: "208px", border: "solid #495057 1px"}}>
            <div style={{textAlign: "center"}}><p>Global Winrate</p></div>
            
            <hr/>
            <Table bordered style={{margin: 0}}>
                <tbody>
                    <tr>
                        <td>Survivor</td>
                        <td>{data["survivor"]}</td>
                        <td>{Math.round(((data["survivor"]/(data["survivor"]+data["kerrigan"])) + Number.EPSILON) * 100)}%</td>
                    </tr>
                    <tr>
                        <td>Kerrigan</td>
                        <td>{data["kerrigan"]}</td>
                        <td>{Math.round(((data["kerrigan"]/(data["survivor"]+data["kerrigan"])) + Number.EPSILON) * 100)}%</td>
                    </tr>
                    <tr>
                        <td>Global</td>
                        <td>{data["survivor"]+data["kerrigan"]}</td>
                        <td>100%</td>
                    </tr>
                </tbody>
            </Table>
        </div>
        </>
    )
}