import Footer from './footer';
import Navigation from './navigation'
import React, { PropsWithChildren } from "react";
 
export default function Layout({ children }: PropsWithChildren) {
  return (
    <>
      <Navigation />
      <main>{children}</main>
      <Footer/>
    </>
  )
}