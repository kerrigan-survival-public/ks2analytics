import * as express from "express"
import { AppDataSource } from "../data-source"
import { Request, Response, NextFunction } from "express"
import { Game } from "../entity/Game"

const GameRouter = express.Router()

GameRouter.route("/count")
    .get(async function (req: Request, res: Response) {
        const repo = AppDataSource.getRepository(Game);
        const kerri_wins = await repo.count({where: {
            outcome: 1
        }});
        const surv_wins = await repo.count({where: {
            outcome: 0
        }});
        res.json({'kerrigan': kerri_wins, 'survivor': surv_wins})
    })

export default GameRouter