import {
    Column,
    Entity,
    PrimaryColumn
} from "typeorm"

@Entity({name: "games"})
export class Game {
    @PrimaryColumn("varchar", { length: 20 })
    game_id: string

    @Column()
    datetime_of_game: Date

    @Column("tinyint")
    timezone: number

    @Column("mediumint")
    duration: number

    @Column("varchar", { length: 64 })
    map_name: string

    @Column("varchar", { length: 13 })
    region: string

    @Column("tinyint")
    outcome: number

    @Column("mediumint")
    rock_percentage: number

    @Column("mediumint")
    anticancer_mean: number

    @Column("tinyint")
    game_mode: number

    @Column("tinyint")
    replay_versopn: number

    @Column("tinyint")
    recorded_game: number

    @Column()
    ds: Date
}