import { DataSource } from "typeorm"
import { Game } from "./entity/Game"

export const AppDataSource = new DataSource({
  type: "mysql",
  host: process.env.MYSQL_HOST,
  port: 3306,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DB,
  synchronize: false, //analytics-api is not authoritative on DB structure.
  logging: true,
  entities: [Game],
  subscribers: [],
  migrations: [],
})