import "reflect-metadata"
import express from 'express';
import cookieParser from 'cookie-parser';
import { AppDataSource } from './data-source';
import GameRouter from "./routes/Game";
import * as cors from 'cors';

const app = express();
app.use(express.json())
app.use(cors.default())

console.log('Initialising Server')

AppDataSource.initialize()
    .then(() => {
        console.log("Data Source has been initialized!");
    })
    .catch((err) => {
        console.error("Error during Data Source initialization:", err)
    })

console.log('Registering Cookie-Parser middleware.');
app.use(cookieParser());
console.log('Cookie-Parser middleware registered.');

app.get('/', (req, res) => {
    res.send('Analytics API is alive.')
})

app.use('/v1/game', GameRouter);

const PORT = 3000;
app.listen(PORT, () => {
    console.log(`analytics-api is running on port ${PORT}.`);
});